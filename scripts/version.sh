#! /bin/sh

export alpine_version=3.14

export ocaml_version=4.12.1

export rust_version=1.52.1

export python_version=3.9.5
